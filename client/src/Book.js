import React from "react";
import { Query } from "react-apollo";
import gql from "graphql-tag";

const Books = props => (
  <Query
    query={gql`
      query Book($title: String!) {
        book(title: $title) {
          title
          author
          cover {
            url
          }
        }
      }
    `}
    variables={{ title: props.title }}
  >
    {({ loading, error, data }) => {
      if (error) return <p>Error :(</p>;

      if (loading) return <div className="spinner" />;

      return (
        <div className="card">
          <img src={data.book.cover.url} alt={`Cover of ${data.book.title}`} />
          <div className="section">
            <ul>
              <li>Title: {data.book.title}</li>
              <li>Author: {data.book.author}</li>
            </ul>
          </div>
        </div>
      );
    }}
  </Query>
);

export default Books;
