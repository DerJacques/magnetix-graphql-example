import React from "react";
import { Query } from "react-apollo";
import gql from "graphql-tag";
import { Link } from "react-router-dom";

const Books = () => (
  <Query
    query={gql`
      {
        books {
          author
          title
        }
      }
    `}
  >
    {({ loading, error, data }) => {
      if (loading) return <div className="spinner" />;
      if (error) return <p>Error :(</p>;

      return (
        <nav>
          {data.books.map(book => (
            <Link key={book.title} to={`/books/${book.title}`}>{`${
              book.author
            }: ${book.title}`}</Link>
          ))}
        </nav>
      );
    }}
  </Query>
);

export default Books;
