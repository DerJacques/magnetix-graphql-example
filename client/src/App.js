import React, { Component } from "react";
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "react-apollo";
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";

import Books from "./Books";
import Book from "./Book";
import CreateBook from "./CreateBook";

const client = new ApolloClient({
  uri: "http://localhost:4000/"
});

class App extends Component {
  render() {
    return (
      <ApolloProvider client={client}>
        <Router>
          <div className="App container">
            <header>
              <Link to={"/"} className="button">
                Books
              </Link>
              <Link to={"/books/create"} className="button">
                Create Book
              </Link>
            </header>
            <Switch>
              <Route exact path="/" component={Books} />
              <Route path="/books/create" component={CreateBook} />
              <Route
                path="/books/:title"
                render={({ match }) => <Book title={match.params.title} />}
              />
            </Switch>
          </div>
        </Router>
      </ApolloProvider>
    );
  }
}

export default App;
