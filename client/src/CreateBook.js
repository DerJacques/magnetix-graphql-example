import React from "react";
import gql from "graphql-tag";
import { Mutation } from "react-apollo";

class CreateBook extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      author: ""
    };
  }

  render() {
    return (
      <Mutation
        mutation={gql`
          mutation AddBook($title: String!, $author: String!) {
            addBook(title: $title, author: $author) {
              title
              author
            }
          }
        `}
      >
        {(addTodo, { data }) => (
          <div>
            <div>
              <label for="title">Title</label>
              <input
                type="text"
                value={this.state.title}
                id="title"
                onChange={e => this.setState({ title: e.target.value })}
              />
            </div>
            <div>
              <label for="author">Author</label>
              <input
                type="text"
                value={this.state.author}
                id="author"
                onChange={e => this.setState({ author: e.target.value })}
              />
            </div>
            <button
              type="submit"
              onClick={() => {
                addTodo({
                  variables: {
                    title: this.state.title,
                    author: this.state.author
                  }
                }).then(() => {
                  window.location.replace("/");
                });
              }}
            >
              Add Book
            </button>
          </div>
        )}
      </Mutation>
    );
  }
}

export default CreateBook;
