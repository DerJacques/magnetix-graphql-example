# Magnetix GraphQL example

A simple web app that allows users to view and add books.
Powered by GraphQL (Apollo) and React.

## Getting started
Clone the repository:  
`$ git clone git@gitlab.com:DerJacques/magnetix-graphql-example.git`

Start server:  
`$ cd server`  
`$ npm i`  
`$ node index.js`  

Start client (separate terminal):  
`$ cd client`  
`$ npm i`  
`$ npm start`  